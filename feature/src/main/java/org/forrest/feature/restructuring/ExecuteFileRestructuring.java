package org.forrest.feature.restructuring;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import org.apache.log4j.Logger;
import org.forrest.api.FileMetaDataConsumer;
import org.forrest.api.metadata.AlreadyExistsFileMetaData;
import org.forrest.api.metadata.DuplicateFileMetaData;
import org.forrest.api.metadata.MoveFileMetaData;
import org.forrest.api.metadata.TransformedFileMetaData;
import org.forrest.api.metadata.TransformedFileMetaDataVisitor;

import io.reactivex.rxjava3.annotations.CheckReturnValue;
import io.reactivex.rxjava3.core.Flowable;

public final class ExecuteFileRestructuring implements FileMetaDataConsumer {

    private final static Logger logger = Logger.getLogger(ExecuteFileRestructuring.class);

    @Override
    public void consume(Flowable<TransformedFileMetaData> fileMetaData) {
        TransformedFileMetaDataVisitor<Void> visitor = new EnactActionVisitorFile();
        fileMetaData.blockingSubscribe(entry -> entry.accept(visitor), logger::error);
    }

    private static final class EnactActionVisitorFile implements TransformedFileMetaDataVisitor<Void> {

        @Override
        public Void visit(MoveFileMetaData entry) {
            try {
                Path newFileDirectory = entry.getNewFileDirectory();
                Path newFileName = entry.getNewFileName();
                Path newLocation = newFileDirectory.resolve(newFileName);
                Files.createDirectories(newFileDirectory);
                Files.move(entry.getOriginalFile(), newLocation);
                logger.debug("Moved " + entry.getOriginalFile() + " to "
                        + entry.getNewFileDirectory().resolve(entry.getNewFileName()));
            } catch (IOException e) {
                logger.error("Failed to move " + entry.getOriginalFile() + " to "
                        + entry.getNewFileDirectory().resolve(entry.getNewFileName()), e);
            }
            return null;
        }

        @Override
        public Void visit(DuplicateFileMetaData entry) {
            try {
                Files.delete(entry.getOriginalFile());
                logger.debug("Deleted duplicate " + entry.getOriginalFile());
            } catch (IOException e) {
                logger.error("Failed to delete " + entry.getOriginalFile(), e);
            }
            return null;
        }

        @Override
        public Void visit(AlreadyExistsFileMetaData entry) {
            // no action required, file already exists
            return null;
        }

    }

}
