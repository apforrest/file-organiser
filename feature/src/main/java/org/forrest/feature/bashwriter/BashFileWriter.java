package org.forrest.feature.bashwriter;

import static java.lang.String.format;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Optional;

import org.apache.log4j.Logger;
import org.forrest.api.FileMetaDataConsumer;
import org.forrest.api.metadata.AlreadyExistsFileMetaData;
import org.forrest.api.metadata.DuplicateFileMetaData;
import org.forrest.api.metadata.FileMetaData;
import org.forrest.api.metadata.MoveFileMetaData;
import org.forrest.api.metadata.TransformedFileMetaData;
import org.forrest.api.metadata.TransformedFileMetaDataVisitor;

import io.reactivex.rxjava3.core.Flowable;

public final class BashFileWriter implements FileMetaDataConsumer {

    private final static Logger logger = Logger.getLogger(BashFileWriter.class);

    private final Path scriptFile;

    public BashFileWriter(Path scriptFile) {
        this.scriptFile = scriptFile;
    }

    @Override
    public void consume(Flowable<TransformedFileMetaData> fileMetaData) {
        TransformedFileMetaDataVisitor<Optional<String>> visitor = new BashVisitorFile();
        fileMetaData.groupBy(FileMetaData::getClass)
                .map(entry -> entry.map(metaData -> metaData.accept(visitor))
                        .filter(Optional::isPresent)
                        .map(Optional::get))
                .toList()
                .flatMapObservable()
                .blockingSubscribe(entries -> Files.write(scriptFile, entries), logger::error);
    }

    private static final class BashVisitorFile implements TransformedFileMetaDataVisitor<Optional<String>> {

        @Override
        public Optional<String> visit(MoveFileMetaData entry) {
            Path originalFile = entry.getOriginalFile();
            Path newFileDirectory = entry.getNewFileDirectory();
            Path newFilePath = newFileDirectory.resolve(entry.getNewFileName());
            return Optional.of(format("mkdir -p \"%s\"; mv \"%s\" \"%s\";", newFileDirectory, originalFile, newFilePath));
        }

        @Override
        public Optional<String> visit(DuplicateFileMetaData entry) {
            return Optional.of(format("rm \"%s\";", entry.getOriginalFile()));
        }

        @Override
        public Optional<String> visit(AlreadyExistsFileMetaData entry) {
            return Optional.empty();
        }

    }

}
