package org.forrest.feature.transformer;

import java.nio.file.Path;
import java.time.LocalDateTime;
import java.util.function.Supplier;

import org.forrest.api.FileMetaDataTransformer;
import org.forrest.api.metadata.AlreadyExistsFileMetaData;
import org.forrest.api.metadata.DuplicateFileMetaData;
import org.forrest.api.metadata.FileMetaData;
import org.forrest.api.metadata.FileMetaData.Builder;
import org.forrest.api.metadata.MoveFileMetaData;
import org.forrest.api.metadata.TransformedFileMetaData;
import org.forrest.feature.transformer.FileNameResolver.ResolutionMapper;

import io.reactivex.rxjava3.core.Flowable;


public final class DestinationResolverTransformer implements FileMetaDataTransformer {

    private final Path destinationDir;
    private final String prefix;

    public DestinationResolverTransformer(Path destinationDir, String prefix) {
        this.destinationDir = destinationDir;
        this.prefix = prefix;
    }

    @Override
    public Flowable<TransformedFileMetaData> transform(Flowable<FileMetaData> metaData) {
        return metaData.groupBy(entry -> generateNewDestinationPath(destinationDir, entry.getCreationDate()))
                .flatMap(entry -> createReportEntries(entry.getKey(), entry));
    }

    private Path generateNewDestinationPath(Path destinationDirectory, LocalDateTime creationDate) {
        return destinationDirectory.resolve(String.valueOf(creationDate.getYear()))
                .resolve(String.valueOf(creationDate.getMonthValue()));
    }

    private Flowable<TransformedFileMetaData> createReportEntries(Path destinationDir, Flowable<FileMetaData> files) {
        DuplicateTestFactory duplicateTestFactory = new DuplicateTestFactory();
        FileNameResolver resolver = new CreationDateResolver(duplicateTestFactory, destinationDir, prefix);
        return files.map(file -> createReportEntry(file, resolver));
    }

    private TransformedFileMetaData createReportEntry(FileMetaData file, FileNameResolver resolver) {
        ResolutionMapper<TransformedFileMetaData> resolutionMapper = new FileMetaDataResolutionMapper(file);
        return resolver.resolveNewPath(file, resolutionMapper);
    }

    private static class FileMetaDataResolutionMapper implements ResolutionMapper<TransformedFileMetaData> {

        private final FileMetaData metaData;

        public FileMetaDataResolutionMapper(FileMetaData metaData) {
            this.metaData = metaData;
        }

        @Override
        public TransformedFileMetaData handleNewPath(Path newFilePath) {
            return populateCommon(MoveFileMetaData::builder)
                    .newFileDirectory(newFilePath.getParent())
                    .newFileName(newFilePath.getFileName())
                    .build();
        }

        @Override
        public TransformedFileMetaData handleDuplicate(Path duplicateFile) {
            return populateCommon(DuplicateFileMetaData::builder)
                    .duplicateFile(duplicateFile)
                    .build();
        }

        @Override
        public TransformedFileMetaData handleAlreadyExists(Path existingFile) {
            return populateCommon(AlreadyExistsFileMetaData::builder)
                    .existingFile(existingFile)
                    .build();
        }

        private <B extends Builder<B, E>, E extends FileMetaData> B populateCommon(Supplier<B> builder) {
            return builder.get()
                    .creationDate(metaData.getCreationDate())
                    .originalFile(metaData.getOriginalFile())
                    .fileExtension(metaData.getFileExtension())
                    .size(metaData.getSize());
        }

    }

}
