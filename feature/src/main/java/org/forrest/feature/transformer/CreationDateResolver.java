package org.forrest.feature.transformer;

import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Predicate;

import org.forrest.api.metadata.FileMetaData;

import io.vavr.Tuple;
import io.vavr.Tuple2;

final class CreationDateResolver implements FileNameResolver {

    private static final DateTimeFormatter filenameFormatter = new DateTimeFormatterBuilder()
            .appendPattern("yyyyMMddHHmmss")
            .toFormatter();

    private final DuplicateTestFactory duplicateTestFactory;
    private final Path destinationDirectory;
    private final String prefix;
    private final Map<Path, Tuple2<Path, Predicate<FileMetaData>>> usedFileNames;

    public CreationDateResolver(DuplicateTestFactory duplicateTestFactory, Path destinationDirectory, String prefix) {
        this.duplicateTestFactory = duplicateTestFactory;
        this.destinationDirectory = destinationDirectory;
        this.prefix = prefix;
        usedFileNames = new HashMap<>();
    }

    @Override
    public <R> R resolveNewPath(FileMetaData metaData, ResolutionMapper<R> mapper) {
        String proposedFileName = formatFileName(metaData.getCreationDate(), prefix);
        return resolveNewPath0(proposedFileName, metaData, 0, mapper);
    }

    private <R> R resolveNewPath0(String proposedFileName, FileMetaData metaData, int count, ResolutionMapper<R> mapper) {
        String proposedFileNameWithCount = (count > 0) ? postfixCount(proposedFileName, count) : proposedFileName;
        Path proposedPath = destinationDirectory.resolve(proposedFileNameWithCount + '.' + metaData.getFileExtension());

        if (proposedPath.equals(metaData.getOriginalFile())) {
            // file is already in the right location, no action needed
            return mapper.handleAlreadyExists(metaData.getOriginalFile());
        }

        if (usedFileNames.containsKey(proposedPath)) {
            // proposed file path is formerly used, potential duplicate
            return handlePathMatch(proposedFileName, proposedPath, metaData, count, mapper);
        }

        if (Files.exists(proposedPath)) {
            // file already exists at the proposed location, potential duplicate
            return handleFileExists(proposedFileName, proposedPath, metaData, count, mapper);
        }

        // proposed path is unique and good to use
        usedFileNames.put(proposedPath, Tuple.of(metaData.getOriginalFile(), duplicateTestFactory.createFrom(metaData)));
        return mapper.handleNewPath(proposedPath);
    }

    private <R> R handlePathMatch(String proposedFileName, Path proposedPath,
            FileMetaData metaData, int count, ResolutionMapper<R> mapper) {
        Tuple2<Path, Predicate<FileMetaData>> existing = usedFileNames.get(proposedPath);
        return testForDuplicate(existing._2, metaData, existing._1, proposedFileName, count, mapper);
    }

    private <R> R handleFileExists(String proposedFileName, Path proposedPath,
            FileMetaData metaData, int count, ResolutionMapper<R> mapper) {
        Predicate<FileMetaData> duplicateFunc = duplicateTestFactory.createFrom(proposedPath);
        usedFileNames.put(proposedPath, Tuple.of(proposedPath, duplicateFunc));
        return testForDuplicate(duplicateFunc, metaData, proposedPath, proposedFileName, count, mapper);
    }

    private <R> R testForDuplicate(Predicate<FileMetaData> duplicatePredicate, FileMetaData metaData,
            Path originalFile, String proposedFileName, int count, ResolutionMapper<R> mapper) {
        if (duplicatePredicate.test(metaData)) {
            // duplicate file found, consider removing
            return mapper.handleDuplicate(originalFile);
        } else {
            // not duplicate, just name clash, increment postfix count
            return resolveNewPath0(proposedFileName, metaData, count + 1, mapper);
        }
    }

    private String postfixCount(String suggestedName, int count) {
        return suggestedName + '(' + count + ')';
    }

    private static String formatFileName(LocalDateTime creationDate, String prefix) {
        return (prefix == null) ? filenameFormatter.format(creationDate) : prefix + filenameFormatter.format(creationDate);
    }

}
