package org.forrest.feature.transformer;

import java.nio.file.Path;

import org.forrest.api.metadata.FileMetaData;

interface FileNameResolver {

    <R> R resolveNewPath(FileMetaData metaData, ResolutionMapper<R> mapper);

    interface ResolutionMapper<R> {

        R handleNewPath(Path newFilePath);

        R handleDuplicate(Path duplicateFile);

        R handleAlreadyExists(Path existingFile);

    }
}
