package org.forrest.feature.transformer;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.function.Predicate;

import org.forrest.api.metadata.FileMetaData;

final class DuplicateTestFactory {

    Predicate<FileMetaData> createFrom(FileMetaData metaData) {
        return otherMetaData -> metaData.getSize() == otherMetaData.getSize();
    }

    Predicate<FileMetaData> createFrom(Path existingFile) {
        return otherMetaData -> getFileSize(existingFile) == otherMetaData.getSize();
    }

    private long getFileSize(Path file) {
        try {
            return Files.size(file);
        } catch (IOException e) {
            throw new RuntimeException("Unable to read file size", e);
        }
    }

}
