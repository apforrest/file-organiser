package org.forrest.feature.csv;

import java.nio.file.Path;
import java.time.LocalDateTime;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Stream;

import org.forrest.api.metadata.AlreadyExistsFileMetaData;
import org.forrest.api.metadata.DuplicateFileMetaData;
import org.forrest.api.metadata.FileMetaData;
import org.forrest.api.metadata.MoveFileMetaData;

enum InternalFactory {

    MOVE(MoveFileMetaData.class,
            line -> populateCommonFields(MoveFileMetaData::builder, line)
                    .newFileDirectory(line.get(CsvLine.CsvColumn.DIRECTORY)
                            .map(Path::of)
                            .orElseThrow())
                    .newFileName(line.get(CsvLine.CsvColumn.FILENAME)
                            .map(Path::of)
                            .orElseThrow())
                    .build()
    ),

    DUPLICATE(DuplicateFileMetaData.class,
            line -> populateCommonFields(DuplicateFileMetaData::builder, line)
                    .duplicateFile(line.get(CsvLine.CsvColumn.MESSAGE)
                            .map(Path::of)
                            .orElseThrow())
                    .build()
    ),

    ALREADY_EXISTS(AlreadyExistsFileMetaData.class,
            line -> populateCommonFields(AlreadyExistsFileMetaData::builder, line)
                    .existingFile(line.get(CsvLine.CsvColumn.MESSAGE)
                            .map(Path::of)
                            .orElseThrow())
                    .build()
    );

    private final Class<? extends FileMetaData> type;
    private final Function<CsvLine, FileMetaData> deserializeEntry;

    InternalFactory(Class<? extends FileMetaData> type, Function<CsvLine, FileMetaData> deserializeEntry) {
        this.type = type;
        this.deserializeEntry = deserializeEntry;
    }

    FileMetaData createEntry(CsvLine columns) {
        return deserializeEntry.apply(columns);
    }

    static Optional<FileMetaData> createFrom(CsvLine line) {
        String typeValue = line.get(CsvLine.CsvColumn.TYPE)
                .orElseThrow();
        return Stream.of(InternalFactory.values())
                .filter(enumType -> typeValue.equals(enumType.toString()))
                .findFirst()
                .map(type -> type.createEntry(line));
    }

    static InternalFactory getType(Class<? extends FileMetaData> classType) {
        return Stream.of(InternalFactory.values())
                .filter(enumType -> classType.isAssignableFrom(enumType.type))
                .findFirst()
                .orElseThrow(() -> new RuntimeException("Impossible for type not to exist " + classType));
    }

    private static <B extends FileMetaData.Builder<B, E>, E extends FileMetaData> B populateCommonFields(
            Supplier<B> builder, CsvLine line) {
        return builder.get()
                .creationDate(line.get(CsvLine.CsvColumn.DATE)
                        .map(value -> LocalDateTime.parse(value, CsvLine.dateFormatter))
                        .orElseThrow())
                .originalFile(line.get(CsvLine.CsvColumn.ORIGINAL)
                        .map(Path::of)
                        .orElseThrow())
                .fileExtension(line.get(CsvLine.CsvColumn.EXTENSION)
                        .orElseThrow())
                .size(line.get(CsvLine.CsvColumn.SIZE)
                        .map(Long::parseLong)
                        .orElseThrow());
    }
}
