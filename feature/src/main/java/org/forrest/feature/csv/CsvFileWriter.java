package org.forrest.feature.csv;

import java.nio.file.Files;
import java.nio.file.Path;

import org.apache.log4j.Logger;
import org.forrest.api.FileMetaDataConsumer;
import org.forrest.api.metadata.AlreadyExistsFileMetaData;
import org.forrest.api.metadata.DuplicateFileMetaData;
import org.forrest.api.metadata.FileMetaData;
import org.forrest.api.metadata.MoveFileMetaData;
import org.forrest.api.metadata.TransformedFileMetaData;
import org.forrest.api.metadata.TransformedFileMetaDataVisitor;

import io.reactivex.rxjava3.core.Flowable;

public final class CsvFileWriter implements FileMetaDataConsumer {

    private final static Logger logger = Logger.getLogger(CsvFileWriter.class);

    private final Path csvFile;

    public CsvFileWriter(Path csvFile) {
        this.csvFile = csvFile;
    }

    @Override
    public void consume(Flowable<TransformedFileMetaData> fileMetaData) {
        TransformedFileMetaDataVisitor<String> visitor = new CsvFileMetaDataVisitor();
        fileMetaData.groupBy(TransformedFileMetaData::getClass)
                .flatMapSingle(entry -> entry.map(metaData -> metaData.accept(visitor)).toList())
                .blockingSubscribe(entries -> Files.write(csvFile, entries), logger::error);
    }

    private static final class CsvFileMetaDataVisitor implements TransformedFileMetaDataVisitor<String> {

        @Override
        public String visit(MoveFileMetaData entry) {
            return populateCommonFields(CsvLine.create(), entry)
                    .set(CsvLine.CsvColumn.DIRECTORY, entry.getNewFileDirectory().toString())
                    .set(CsvLine.CsvColumn.FILENAME, entry.getNewFileName().toString())
                    .serialise();
        }

        @Override
        public String visit(DuplicateFileMetaData entry) {
            return populateCommonFields(CsvLine.create(), entry)
                    .set(CsvLine.CsvColumn.MESSAGE, entry.getDuplicateFile().toString())
                    .serialise();
        }

        @Override
        public String visit(AlreadyExistsFileMetaData entry) {
            return populateCommonFields(CsvLine.create(), entry)
                    .set(CsvLine.CsvColumn.MESSAGE, entry.getExistingFile().toString())
                    .serialise();
        }

        private static CsvLine populateCommonFields(CsvLine line, FileMetaData reportEntry) {
            return line.set(CsvLine.CsvColumn.DATE, CsvLine.dateFormatter.format(reportEntry.getCreationDate()))
                    .set(CsvLine.CsvColumn.ORIGINAL, reportEntry.getOriginalFile().toString())
                    .set(CsvLine.CsvColumn.EXTENSION, reportEntry.getFileExtension())
                    .set(CsvLine.CsvColumn.SIZE, String.valueOf(reportEntry.getSize()))
                    .set(CsvLine.CsvColumn.TYPE, InternalFactory.getType(reportEntry.getClass()).toString());
        }

    }

}
