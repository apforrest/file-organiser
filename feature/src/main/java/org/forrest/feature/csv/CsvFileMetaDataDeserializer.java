package org.forrest.feature.csv;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Optional;
import java.util.stream.Stream;

import org.forrest.api.FileMetaDataProducer;
import org.forrest.api.metadata.FileMetaData;

import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.schedulers.Schedulers;

public final class CsvFileMetaDataDeserializer implements FileMetaDataProducer {

    private final Path sourceFile;

    public CsvFileMetaDataDeserializer(Path sourceFile) {
        this.sourceFile = sourceFile;
    }

    @Override
    public Flowable<FileMetaData> produce() {
        Flowable<String> lines = Flowable.using(() -> Files.lines(sourceFile),
                stream -> Flowable.fromIterable(stream::iterator),
                Stream::close);

        return lines.subscribeOn(Schedulers.io())
                .map(this::deserialize)
                .filter(Optional::isPresent)
                .map(Optional::get);
    }

    private Optional<FileMetaData> deserialize(String line) {
        return InternalFactory.createFrom(CsvLine.deserialize(line));
    }

}
