package org.forrest.feature.csv;

import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toMap;

import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.google.common.base.Splitter;

import io.vavr.Tuple;

public final class CsvLine {

    public static final DateTimeFormatter dateFormatter = new DateTimeFormatterBuilder()
            .appendPattern("dd/MM/yyyy HH:mm:ss")
            .toFormatter();

    enum CsvColumn {

        DATE,
        ORIGINAL,
        EXTENSION,
        SIZE,
        DIRECTORY,
        FILENAME,
        TYPE,
        MESSAGE;

    }

    private final Map<CsvColumn, String> columns;

    private CsvLine() {
        this.columns = new HashMap<>();
    }

    private CsvLine(Map<CsvColumn, String> columns) {
        this.columns = columns;
    }

    CsvLine set(CsvColumn column, String value) {
        columns.put(column, value);
        return this;
    }

    Optional<String> get(CsvColumn column) {
        return Optional.ofNullable(columns.get(column));
    }

    String serialise() {
        return Arrays.stream(CsvColumn.values())
                .map(column -> columns.getOrDefault(column, ""))
                .collect(joining(","));
    }

    static CsvLine create() {
        return new CsvLine();
    }

    static CsvLine deserialize(String line) {
        List<String> values = Splitter.on(',')
                .splitToList(line);

        if (values.size() < CsvColumn.values().length) {
            throw new IllegalStateException("The CSV line has more columns than that are known about");
        }

        Map<CsvColumn, String> columns = Arrays.stream(CsvColumn.values())
                .map(column -> Tuple.of(column, values.get(column.ordinal())))
                .collect(toMap(t -> t._1, t -> t._2));
        return new CsvLine(columns);
    }

}
