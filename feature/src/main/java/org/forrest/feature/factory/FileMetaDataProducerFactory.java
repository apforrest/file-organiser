package org.forrest.feature.factory;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.apache.log4j.Logger;
import org.forrest.api.FileMetaDataProducer;
import org.forrest.feature.csv.CsvFileMetaDataDeserializer;
import org.forrest.feature.dirreader.DirectoryReadFileMetaDataProducer;

public final class FileMetaDataProducerFactory {

    private final static Logger logger = Logger.getLogger(FileMetaDataProducerFactory.class);

    public FileMetaDataProducer create(CommandLine params) {
        if (!params.hasOption('s')) {
            throw new IllegalArgumentException("No source has been provided (-s)");
        }

        Path source = Path.of(params.getOptionValue('s'));

        if (Files.notExists(source)) {
            throw new IllegalArgumentException("The source path does not exist, " + source);
        }

        if (Files.isRegularFile(source)) {
            logger.debug("Created CSV file deserializer producer");
            return new CsvFileMetaDataDeserializer(source);
        }

        if (!Files.isDirectory(source)) {
            throw new IllegalArgumentException("Unable to understand passed source, " + source);
        }

        String[] filter = params.getOptionValues('f');

        logger.debug("Created directory reader producer");
        return new DirectoryReadFileMetaDataProducer(source, filter == null ? List.of() : List.of(filter));
    }

    public List<Option> registerCmdLineOptions() {
        return List.of(Option.builder("s")
                        .longOpt("source")
                        .type(String.class)
                        .argName("path")
                        .hasArg()
                        .desc("Source location for photos to restructure or the source CSV file")
                        .required()
                        .build(),
                Option.builder("f")
                        .longOpt("filter")
                        .type(String.class)
                        .argName("extensions")
                        .hasArgs()
                        .desc("Common separated list of file extensions to filter in")
                        .build());
    }

}
