package org.forrest.feature.factory;

import java.nio.file.Path;
import java.util.List;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.apache.log4j.Logger;
import org.forrest.api.FileMetaDataTransformer;
import org.forrest.feature.transformer.DestinationResolverTransformer;

public final class FileMetaDataTransformerFactory {

    private final static Logger logger = Logger.getLogger(FileMetaDataTransformerFactory.class);

    public FileMetaDataTransformer create(CommandLine params) {
        if (!params.hasOption('d')) {
            throw new IllegalArgumentException("No destination has been provided (-d)");
        }

        Path destination = Path.of(params.getOptionValue('d'));
        String prefix = params.getOptionValue('p');

        logger.debug("Created directory reader producer");
        return new DestinationResolverTransformer(destination, prefix);
    }

    public List<Option> registerCmdLineOptions() {
        return List.of(Option.builder("d")
                        .longOpt("destination")
                        .type(String.class)
                        .argName("path")
                        .hasArg()
                        .desc("Destination location for photos to restructure")
                        .build(),
                Option.builder("p")
                        .longOpt("prefix")
                        .type(String.class)
                        .argName("string")
                        .hasArgs()
                        .desc("Prefix string to be added to the beginning of renamed files")
                        .build());
    }

}
