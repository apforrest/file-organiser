package org.forrest.feature.factory;

import java.nio.file.Path;
import java.util.List;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.apache.log4j.Logger;
import org.forrest.api.FileMetaDataConsumer;
import org.forrest.feature.bashwriter.BashFileWriter;
import org.forrest.feature.csv.CsvFileWriter;
import org.forrest.feature.log.LogWriter;
import org.forrest.feature.restructuring.ExecuteFileRestructuring;

public final class FileMetaDataConsumerFactory {

    private final static Logger logger = Logger.getLogger(FileMetaDataConsumerFactory.class);

    public FileMetaDataConsumer create(CommandLine params) {
        if (params.hasOption('x')) {
            logger.debug("Created file restructure consumer");
            return new ExecuteFileRestructuring();
        } else if (params.hasOption('c')) {
            logger.debug("Created CSV file writer consumer");
            return new CsvFileWriter(Path.of(params.getOptionValue('c')));
        } else if (params.hasOption('b')) {
            logger.debug("Created bash script writer consumer");
            return new BashFileWriter(Path.of(params.getOptionValue('b')));
        } else {
            logger.debug("Created log consumer");
            return new LogWriter();
        }
    }

    public List<Option> registerCmdLineOptions() {
        return List.of(Option.builder("x")
                        .longOpt("execute")
                        .type(String.class)
                        .argName("path")
                        .desc("Execute the restructure commands")
                        .build(),
                Option.builder("c")
                        .longOpt("csv")
                        .type(String.class)
                        .argName("path")
                        .hasArg()
                        .desc("Location of the generated csv file")
                        .build(),
                Option.builder("b")
                        .longOpt("bash")
                        .type(String.class)
                        .argName("path")
                        .hasArg()
                        .desc("Location of the generated bash file")
                        .build());
    }

}
