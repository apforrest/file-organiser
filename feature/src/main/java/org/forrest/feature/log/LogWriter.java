package org.forrest.feature.log;

import java.util.Comparator;

import org.apache.log4j.Logger;
import org.forrest.api.FileMetaDataConsumer;
import org.forrest.api.metadata.AlreadyExistsFileMetaData;
import org.forrest.api.metadata.DuplicateFileMetaData;
import org.forrest.api.metadata.MoveFileMetaData;
import org.forrest.api.metadata.TransformedFileMetaData;
import org.forrest.api.metadata.TransformedFileMetaDataVisitor;

import io.reactivex.rxjava3.core.Flowable;

public final class LogWriter implements FileMetaDataConsumer {

    private final static Logger logger = Logger.getLogger(LogWriter.class);

    @Override
    public void consume(Flowable<TransformedFileMetaData> fileMetaData) {
        TransformedFileMetaDataVisitor<Void> visitor = new LogVisitor();
        fileMetaData.sorted(Comparator.comparing(test -> test.getClass().getName()))
                .blockingSubscribe(entry -> entry.accept(visitor), logger::error);
    }

    private static class LogVisitor implements TransformedFileMetaDataVisitor<Void> {

        @Override
        public Void visit(MoveFileMetaData moveEntry) {
            logger.debug("Move file " + moveEntry.getOriginalFile() + ", to "
                    + moveEntry.getNewFileDirectory().resolve(moveEntry.getNewFileName()));
            return null;
        }

        @Override
        public Void visit(DuplicateFileMetaData duplicateEntry) {
            logger.debug("Delete file " + duplicateEntry.getOriginalFile()
                    + ", duplicate is " + duplicateEntry.getDuplicateFile());
            return null;
        }

        @Override
        public Void visit(AlreadyExistsFileMetaData alreadyExistsEntry) {
            logger.debug("No action, file already exists in the correct location "
                    + alreadyExistsEntry.getOriginalFile());
            return null;
        }

    }

}
