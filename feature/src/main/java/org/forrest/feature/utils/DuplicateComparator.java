package org.forrest.feature.utils;

import java.util.function.BiPredicate;
import java.util.function.Supplier;

final class DuplicateComparator<C, R> {

    private final C existing;

    private DuplicateComparator(C existing) {
        this.existing = existing;
    }

    Test<C, R> with(C original) {
        return duplicatePredicate -> duplicateSupplier -> nonDuplicateSupplier ->
                duplicatePredicate.test(existing, original) ? duplicateSupplier.get() : nonDuplicateSupplier.get();
    }


    interface Test<C, R> {

        DuplicateFunction<R> using(BiPredicate<C, C> duplicatePredicate);

    }

    interface DuplicateFunction<R> {

        NotDuplicateFunction<R> whenMatch(Supplier<R> duplicateSupplier);

    }

    interface NotDuplicateFunction<R> {

        R otherwise(Supplier<R> nonDuplicateSupplier);

    }

    static <C, R> DuplicateComparator<C, R> compare(C existing) {
        return new DuplicateComparator<>(existing);
    }

}
