package org.forrest.feature.utils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;

public final class FileUtils {

    private FileUtils() {
        throw new RuntimeException();
    }

    public static BasicFileAttributes extractFileAttributes(Path path) {
        try {
            return Files.readAttributes(path, BasicFileAttributes.class);
        } catch (IOException ex) {
            throw new RuntimeException("Failed to read attributes from file " + path, ex);
        }
    }

}
