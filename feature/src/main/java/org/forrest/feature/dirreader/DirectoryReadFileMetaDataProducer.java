package org.forrest.feature.dirreader;

import static com.google.common.io.Files.getFileExtension;
import static org.forrest.api.metadata.BasicFileMetaData.builder;
import static org.forrest.feature.utils.FileUtils.extractFileAttributes;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;
import java.util.stream.Stream;

import org.forrest.api.FileMetaDataProducer;
import org.forrest.api.metadata.FileMetaData;

import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.schedulers.Schedulers;

public final class DirectoryReadFileMetaDataProducer implements FileMetaDataProducer {

    private final Path sourceDir;
    private final List<String> filter;

    public DirectoryReadFileMetaDataProducer(Path sourceDir, List<String> filter) {
        this.sourceDir = sourceDir;
        this.filter = filter;
    }

    @Override
    public Flowable<FileMetaData> produce() {
        Flowable<Path> directoryPaths = Flowable.using(
                () -> Files.walk(sourceDir),
                stream -> Flowable.fromIterable(stream::iterator),
                Stream::close);

        return directoryPaths.subscribeOn(Schedulers.io())
                .filter(Files::isRegularFile)
                .filter(this::existsInFilter)
                .map(this::extractFileData);
    }

    private boolean existsInFilter(Path file) {
        return filter.isEmpty() || filter.contains(getFileExtension(file.toString()).toLowerCase());
    }

    private FileMetaData extractFileData(Path file) {
        BasicFileAttributes attributes = extractFileAttributes(file);
        Instant creationDateInstant = attributes.creationTime().toInstant();
        LocalDateTime creationDate = LocalDateTime.ofInstant(creationDateInstant, ZoneOffset.UTC);
        String extension = getFileExtension(file.toString()).toLowerCase();

        return builder()
                .creationDate(creationDate)
                .originalFile(file)
                .fileExtension(extension)
                .size(attributes.size())
                .build();
    }

}
