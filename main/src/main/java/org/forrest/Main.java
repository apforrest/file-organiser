package org.forrest;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.log4j.BasicConfigurator;
import org.forrest.api.FileMetaDataConsumer;
import org.forrest.api.FileMetaDataProducer;
import org.forrest.api.FileMetaDataTransformer;
import org.forrest.feature.factory.FileMetaDataConsumerFactory;
import org.forrest.feature.factory.FileMetaDataProducerFactory;
import org.forrest.feature.factory.FileMetaDataTransformerFactory;

public final class Main {

    public static void main(String... args) throws ParseException {
        BasicConfigurator.configure();

        FileMetaDataProducerFactory producerFactory = new FileMetaDataProducerFactory();
        FileMetaDataConsumerFactory consumerFactory = new FileMetaDataConsumerFactory();
        FileMetaDataTransformerFactory transformerFactory = new FileMetaDataTransformerFactory();

        Options options = new Options();
        producerFactory.registerCmdLineOptions().forEach(options::addOption);
        consumerFactory.registerCmdLineOptions().forEach(options::addOption);
        transformerFactory.registerCmdLineOptions().forEach(options::addOption);

        try {
            CommandLineParser parser = new DefaultParser();
            CommandLine commandLine = parser.parse(options, args);

            FileMetaDataProducer producer = producerFactory.create(commandLine);
            FileMetaDataTransformer transformer = transformerFactory.create(commandLine);
            FileMetaDataConsumer consumer = consumerFactory.create(commandLine);

            consumer.consume(transformer.transform(producer.produce()));
        } catch (ParseException pE) {
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("File Organiser", options);
            throw pE;
        }
    }

}
