package org.forrest.api;

import org.forrest.api.metadata.TransformedFileMetaData;

import io.reactivex.rxjava3.core.Flowable;

public interface FileMetaDataConsumer {

    void consume(Flowable<TransformedFileMetaData> fileMetaData);

}
