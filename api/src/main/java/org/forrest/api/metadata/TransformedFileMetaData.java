package org.forrest.api.metadata;

public abstract class TransformedFileMetaData extends FileMetaData {

    protected TransformedFileMetaData(Builder<?, ?> builder) {
        super(builder);
    }

    public abstract <T> T accept(TransformedFileMetaDataVisitor<T> visitor);

}
