package org.forrest.api.metadata;

public final class BasicFileMetaData extends FileMetaData {

    private BasicFileMetaData(BasicBuilder builder) {
        super(builder);
    }

    public static final class BasicBuilder extends Builder<BasicBuilder, BasicFileMetaData> {

        private BasicBuilder() {
        }

        @Override
        protected BasicBuilder self() {
            return this;
        }

        @Override
        public BasicFileMetaData build() {
            return new BasicFileMetaData(this);
        }

    }

    public static BasicBuilder builder() {
        return new BasicBuilder();
    }

}
