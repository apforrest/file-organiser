package org.forrest.api.metadata;

import java.nio.file.Path;

public final class DuplicateFileMetaData extends TransformedFileMetaData {

    private final Path duplicateFile;

    private DuplicateFileMetaData(DuplicateBuilder builder) {
        super(builder);
        duplicateFile = builder.duplicateFile;
    }

    public Path getDuplicateFile() {
        return duplicateFile;
    }

    @Override
    public <T> T accept(TransformedFileMetaDataVisitor<T> visitor) {
        return visitor.visit(this);
    }

    public static final class DuplicateBuilder extends Builder<DuplicateBuilder, DuplicateFileMetaData> {

        private Path duplicateFile;

        private DuplicateBuilder() {
        }

        public DuplicateBuilder duplicateFile(Path duplicateFile) {
            this.duplicateFile = duplicateFile;
            return this;
        }

        @Override
        protected DuplicateBuilder self() {
            return this;
        }

        @Override
        public DuplicateFileMetaData build() {
            return new DuplicateFileMetaData(this);
        }

    }

    public static DuplicateBuilder builder() {
        return new DuplicateBuilder();
    }

}
