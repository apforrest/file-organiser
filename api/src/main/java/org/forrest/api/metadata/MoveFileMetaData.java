package org.forrest.api.metadata;

import java.nio.file.Path;

public final class MoveFileMetaData extends TransformedFileMetaData {

    private final Path newFileDirectory;
    private final Path newFileName;

    private MoveFileMetaData(MoveBuilder builder) {
        super(builder);
        newFileDirectory = builder.newFileDirectory;
        newFileName = builder.newFileName;
    }

    public Path getNewFileDirectory() {
        return newFileDirectory;
    }

    public Path getNewFileName() {
        return newFileName;
    }

    @Override
    public <T> T accept(TransformedFileMetaDataVisitor<T> visitor) {
        return visitor.visit(this);
    }

    public static final class MoveBuilder extends Builder<MoveBuilder, MoveFileMetaData> {

        private Path newFileDirectory;
        private Path newFileName;

        private MoveBuilder() {
        }

        public MoveBuilder newFileDirectory(Path newFileDirectory) {
            this.newFileDirectory = newFileDirectory;
            return this;
        }

        public MoveBuilder newFileName(Path newFileName) {
            this.newFileName = newFileName;
            return this;
        }

        @Override
        protected MoveBuilder self() {
            return this;
        }

        @Override
        public MoveFileMetaData build() {
            return new MoveFileMetaData(this);
        }

    }

    public static MoveBuilder builder() {
        return new MoveBuilder();
    }

}
