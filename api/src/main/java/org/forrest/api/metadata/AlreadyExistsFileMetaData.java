package org.forrest.api.metadata;

import java.nio.file.Path;

public final class AlreadyExistsFileMetaData extends TransformedFileMetaData {

    private final Path existingFile;

    private AlreadyExistsFileMetaData(AlreadyExistsBuilder builder) {
        super(builder);
        existingFile = builder.existingFile;
    }

    public Path getExistingFile() {
        return existingFile;
    }

    @Override
    public <T> T accept(TransformedFileMetaDataVisitor<T> visitor) {
        return visitor.visit(this);
    }

    public static final class AlreadyExistsBuilder extends Builder<AlreadyExistsBuilder, AlreadyExistsFileMetaData> {

        private Path existingFile;

        private AlreadyExistsBuilder() {
        }

        public AlreadyExistsBuilder existingFile(Path existingFile) {
            this.existingFile = existingFile;
            return this;
        }

        @Override
        protected AlreadyExistsBuilder self() {
            return this;
        }

        @Override
        public AlreadyExistsFileMetaData build() {
            return new AlreadyExistsFileMetaData(this);
        }

    }

    public static AlreadyExistsBuilder builder() {
        return new AlreadyExistsBuilder();
    }

}
