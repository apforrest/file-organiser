package org.forrest.api.metadata;

import java.nio.file.Path;
import java.time.LocalDateTime;

public abstract class FileMetaData {

    private final LocalDateTime creationDate;
    private final Path originalFile;
    private final String fileExtension;
    private final long size;

    protected FileMetaData(Builder<?, ?> builder) {
        creationDate = builder.creationDate;
        originalFile = builder.originalFile;
        fileExtension = builder.fileExtension;
        size = builder.size;
    }

    public final LocalDateTime getCreationDate() {
        return creationDate;
    }

    public final Path getOriginalFile() {
        return originalFile;
    }

    public final String getFileExtension() {
        return fileExtension;
    }

    public final long getSize() {
        return size;
    }

    public abstract static class Builder<B extends Builder<B, E>, E extends FileMetaData> {

        private LocalDateTime creationDate;
        private Path originalFile;
        private String fileExtension;
        private long size;

        public final B creationDate(LocalDateTime creationDate) {
            this.creationDate = creationDate;
            return self();
        }

        public final B originalFile(Path originalFile) {
            this.originalFile = originalFile;
            return self();
        }

        public final B fileExtension(String fileExtension) {
            this.fileExtension = fileExtension;
            return self();
        }

        public final B size(long size) {
            this.size = size;
            return self();
        }

        protected abstract B self();

        public abstract E build();

    }
}
