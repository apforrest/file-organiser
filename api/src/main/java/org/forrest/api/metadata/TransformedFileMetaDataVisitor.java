package org.forrest.api.metadata;

public interface TransformedFileMetaDataVisitor<T> {

    T visit(MoveFileMetaData moveEntry);

    T visit(DuplicateFileMetaData duplicateEntry);

    T visit(AlreadyExistsFileMetaData alreadyExistsEntry);

}
