package org.forrest.api;

import org.forrest.api.metadata.FileMetaData;
import org.forrest.api.metadata.TransformedFileMetaData;

import io.reactivex.rxjava3.core.Flowable;

public interface FileMetaDataTransformer {

    Flowable<TransformedFileMetaData> transform(Flowable<FileMetaData> metaData);

}
