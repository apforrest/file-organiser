package org.forrest.api;

import org.forrest.api.metadata.FileMetaData;

import io.reactivex.rxjava3.core.Flowable;

public interface FileMetaDataProducer {

    Flowable<FileMetaData> produce();

}
